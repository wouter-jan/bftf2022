function ProgramView() {
    return (
        <div className="program-view-container">
        <p>
            <code>The goal of the program is to change the world! No joke!</code>
        </p>
        <p>
            <code>How we want to achieve that is another question, that we are still very much debating.</code>
        </p>
        <p>
            <code>More soon!</code>
        </p>
        </div>
    )
}

export default ProgramView;
