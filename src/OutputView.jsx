function OutputView() {
    return (
        <div className="output-view-container">
            <p>
                <code>Here comes an action plan on how to change the world, based on what was discussed in the program.</code>
            </p>
        </div>
    )
}

export default OutputView;