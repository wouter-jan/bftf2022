function PrivacyView() {
    return (
        <div className="privacy-view-container">
            <p>
                <code>We do not gather or process any cookies, leave alone process personal data (to be reviewed).</code>
            </p>
        </div>
    )
}

export default PrivacyView;