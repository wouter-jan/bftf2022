import picture from './brusselsPics/image2.jpeg'

function BrusselsView() {
    return (

        <div className="brussels-view-container">
            <div className="gallery-container">
                <img src={picture} alt="Brussels is beautiful" />
            </div>
        </div>
    )
}

export default BrusselsView;