function AboutUsView() {
    return (
        <div className="about-us-container">
            <p>
                <code>We are the 2012 BFTF alumni and we're trying to pull this off.</code> 
            </p>
            <p>
                <code>More on all of us later once we managed to get this funded.</code> 
            </p>
        </div>
    )
} 

export default AboutUsView;