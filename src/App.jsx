import { useState } from 'react';
import AboutUsView from './AboutUsView';
import './App.scss';
import BrusselsView from './BrusselsView';
import HomeView from './HomeView';
import OutputView from './OutputView';
import PrivacyView from './PrivacyView';
import ProgramView from './ProgramView';

function App() {

  const [view, setView] = useState('homeView');

  function contactUs() {
    window.location = "mailto:wjleys@gmail.com";
  }

  function setClassNames(manualClassNames, dynamicClassNames) {
    return `${manualClassNames} ${dynamicClassNames}`
  }

  function tagActive(viewName) {
    return view === viewName && 'active'
  }

  return (
    <div className="App">
      <div className="content">
        <div className="header">
          <div className={setClassNames("header-item", tagActive('homeView'))} onClick={() => setView('homeView')}>
            🏠 Home
          </div>
          <div className={setClassNames("header-item", tagActive('aboutUsView'))} onClick={() => setView('aboutUsView')}>
            👋 About us
          </div>
          <div className={setClassNames("header-item", tagActive('programView'))} onClick={() => setView('programView')}>
            🚀 Program
          </div>
          <div className={setClassNames("header-item", tagActive('outputView'))} onClick={() => setView('outputView')}>
            🪂 Output
          </div>
        </div>
        <div className="view-container">
        {view === 'homeView' && <HomeView />}
        {view === 'aboutUsView' && <AboutUsView />}
        {view === 'programView' && <ProgramView />}
        {view === 'outputView' && <OutputView />}
        {view === 'privacyView' && <PrivacyView />}
        {view === 'brusselsView' && <BrusselsView />}
        </div>
        <div className="footer">
          <div onClick={() => contactUs()} className="footer-item">
            📫 Contact us
          </div>
          <div className={setClassNames("footer-item", tagActive('privacyView'))} onClick={() => setView('privacyView')}>
            🔒 Privacy
          </div>
          <div className={setClassNames("footer-item", tagActive('brusselsView'))} onClick={() => setView('brusselsView')}>
            🌆 Brussels is Beautiful
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
