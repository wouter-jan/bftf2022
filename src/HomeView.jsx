import Atomium from './Atomium.jsx';

function HomeView() {
    return (
      <>
        <div className="atomium">
        <Atomium />
      </div>
      <div className="country-flags-main">
      🇧🇪 🇺🇸 🇪🇺
      </div>
      <div className="conference-title">
        BFTF in Brussels 2022
      </div>
      <div className="country-flags-other">
      🇦🇱 🇦🇲 🇦🇹 🇦🇿 🇧🇦 🇭🇷 🇨🇾 🇨🇿 🇩🇰 🇫🇮 
      🇫🇷 🇬🇪 🇩🇪 🇬🇷 🇭🇺 🇮🇸 🇮🇹 🇽🇰 🇱🇻 🇱🇹
      🇱🇺 🇲🇹 🇲🇩 🇲🇪 🇳🇱 🇳🇴 🇵🇱 🇵🇹 🇷🇴 🇷🇺
      🇷🇸 🇸🇰 🇸🇮 🇪🇸 🇨🇭 🇹🇷 🇺🇦 🇬🇧
      </div>
      <div className="loading-message">
        <p><code>Under construction </code> </p>
        <div className="loader-container">
          <div className="loader"></div>
          <div>
            <code>Eagerly awaiting funding ...</code>
          </div>
        </div>
      </div>
    </>
    )
}

export default HomeView;